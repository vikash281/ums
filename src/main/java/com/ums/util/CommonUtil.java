package com.ums.util;

import com.ums.domain.UserEntity;
import com.ums.dto.User;
import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class CommonUtil {
    private static Logger logger = LoggerFactory.getLogger(CommonUtil.class);

    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM-dd-yyyy");

    public UserEntity convertToUserEntity(User user) throws ParseException {
        UserEntity userEntity = new UserEntity();
        userEntity.setUserName(user.getUserName());
        userEntity.setPassword(user.getPassword());
        userEntity.setFirstName(user.getFirstName());
        userEntity.setLastname(user.getLastname());
        userEntity.setEmail(user.getEmail());
        userEntity.setDob(simpleDateFormat.parse(user.getDob()));
        return userEntity;
    }

    public User convertToUser(UserEntity userEntity, String password) throws ParseException {
        User user = new User();
        user.setId(userEntity.getId());
        user.setUserName(userEntity.getUserName());
        user.setPassword(password);
        user.setFirstName(userEntity.getFirstName());
        user.setLastname(userEntity.getLastname());
        user.setEmail(userEntity.getEmail());
        user.setDob(simpleDateFormat.format(userEntity.getDob()));
        return user;
    }

    public User convertToUser(UserEntity userEntity) throws ParseException {
        return convertToUser(userEntity, null);
    }

    public static String generateCommonLangPassword() {
        logger.info("generateCommonLangPassword started ");

        String upperCaseLetters = RandomStringUtils.random(2, 65, 90, true, true);
        String lowerCaseLetters = RandomStringUtils.random(2, 97, 122, true, true);
        String numbers = RandomStringUtils.randomNumeric(1);
        String specialChar = RandomStringUtils.random(1, 33, 47, false, false);
        String totalChars = RandomStringUtils.randomAlphanumeric(2);

        String combinedChars = upperCaseLetters.concat(lowerCaseLetters)
                .concat(numbers)
                .concat(specialChar)
                .concat(totalChars);

        List<Character> pwdChars = combinedChars.chars()
                .mapToObj(c -> (char) c)
                .collect(Collectors.toList());

        Collections.shuffle(pwdChars);

        String password = pwdChars.stream()
                .collect(StringBuilder::new, StringBuilder::append, StringBuilder::append)
                .toString();

        logger.info("generateCommonLangPassword ended");
        return password;
    }

    public UserEntity prepareUserEntityForUpdate(UserEntity userEntity, User user) throws ParseException {
        logger.info("prepareUserEntityForUpdate started ");
        if (user.getFirstName() != null && !userEntity.getFirstName().equals(user.getFirstName())) {
            userEntity.setFirstName(user.getFirstName());
        }
        if (user.getLastname() != null && !userEntity.getLastname().equals(user.getLastname())) {
            userEntity.setFirstName(user.getLastname());
        }
        if (user.getEmail() != null && !userEntity.getEmail().equals(user.getEmail())) {
            userEntity.setEmail(user.getEmail());
        }

        Date dobForUpdate = simpleDateFormat.parse(user.getDob());
        if (user.getDob() != null && !userEntity.getDob().equals(dobForUpdate)) {
            userEntity.setDob(dobForUpdate);
        }
        logger.info("prepareUserEntityForUpdate ended ");

        return userEntity;
    }

    public static String get_SHA_512_SecurePassword(String passwordToHash) {
        logger.info("get_SHA_512_SecurePassword started pass: " + passwordToHash);
        String generatedPassword = null;
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-512");
            // md.update(salt.getBytes(StandardCharsets.UTF_8));
            byte[] bytes = md.digest(passwordToHash.getBytes(StandardCharsets.UTF_8));
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.length; i++) {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            generatedPassword = sb.toString();
        } catch (NoSuchAlgorithmException e) {
            logger.error("get_SHA_512_SecurePassword fail to hash the password: " + passwordToHash, e);

        }
        logger.info("get_SHA_512_SecurePassword end pass" );
        return generatedPassword;
    }

}
