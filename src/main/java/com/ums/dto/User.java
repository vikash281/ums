package com.ums.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@ToString
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class User {
    private Long id;
    @NotNull
    private String userName;
    private String password;
    private String email;
    private String firstName;
    private String lastname;
    private String dob;

}
