package com.ums.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;

@Setter
@Getter
@EqualsAndHashCode
@ToString
public class UpdatePasswordRequest {
    @NotNull(message = "userName can not be null")
    private String userName;
    @NotNull(message = "currentPassword can not be null")
    private String currentPassword;
    @NotNull(message = "newPassword can not be null")
    private String newPassword;


}
