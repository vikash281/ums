package com.ums.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;

@Setter
@Getter
@EqualsAndHashCode
@ToString
public class LoginRequest {
    @NotNull(message = "userName can not be null")
    private String userName;
    @NotNull(message = "password can not be null")
    private String password;

}
