package com.ums.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Setter
@Getter
@EqualsAndHashCode
public class UpdateUserRequest {

    @NotNull(message = "userName can not be null")
    private String userName;
    @NotNull(message = "email can not be null")
    private String email;
    @NotNull(message = "firstName can not be null")
    private String firstName;
    @NotNull(message = "lastName can not be null")
    private String lastName;
    @NotNull(message = "dob can not be null")
    private String dob;

}
