package com.ums.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@Setter
@Getter
@ToString
@EqualsAndHashCode
public class CreateUserRequest {
    @NotNull(message = "userName can not be null")
    private String userName;
    @NotNull(message = "email can not be null")
    @Email
    private String email;
    @NotNull(message = "firstName can not be null")
    private String firstName;
    @NotNull(message = "lastName can not be null")
    private String lastName;
    @NotNull(message = "dob can not be null")
    private String dob;

}
