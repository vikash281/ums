package com.ums.exception;

public class UserNotFoundException extends Exception {

    public UserNotFoundException(String message){
        super(message);
    }

    public UserNotFoundException(String message, Exception e){
        super(message, e);
    }

    public UserNotFoundException(Exception e){
        super(e);
    }
}
