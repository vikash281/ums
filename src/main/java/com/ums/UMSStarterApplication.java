package com.ums;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;



@SpringBootApplication
public class UMSStarterApplication {

	public static void main(String[] args) {
		SpringApplication.run(UMSStarterApplication.class, args);
	}

}
