package com.ums.service.impl;

import com.ums.domain.UserEntity;
import com.ums.dto.User;
import com.ums.exception.AuthenticationException;
import com.ums.exception.ServiceException;
import com.ums.repository.UserRepository;
import com.ums.service.UserService;
import com.ums.util.CommonUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private CommonUtil commonUtil;

    @Override
    public User createUser(User user) throws ServiceException {
        logger.info("createUser() started input :" + user);
        try {
            String password = CommonUtil.generateCommonLangPassword();
            String hashPassword = CommonUtil.get_SHA_512_SecurePassword(password);
            user.setPassword(hashPassword);
            UserEntity userEntity = userRepository.save(commonUtil.convertToUserEntity(user));
            return commonUtil.convertToUser(userEntity, password);
        } catch (ParseException e) {
            logger.error("error to create User " + user, e);
            throw new ServiceException(e);
        }
    }

    @Override
    public User login(String userName, String password) throws ServiceException {
        logger.info("login() started userName :" + userName);
        try {
            UserEntity userEntity = userRepository.findByUserName(userName);
            String hashPassword = commonUtil.get_SHA_512_SecurePassword(password);
            if (hashPassword.equals(userEntity.getPassword())) {
                return commonUtil.convertToUser(userEntity, null);
            } else {
                throw new AuthenticationException("User Credential not Valid");
            }
        } catch (ParseException | AuthenticationException e) {
            logger.error("user login failed userName :" + userName, e);
            throw new ServiceException(e);
        }
    }

    @Override
    public void updatePassword(String userName, String currentPassword, String newPassword) throws ServiceException {
        logger.info("updatePassword() started userName :" + userName);
        try {
            UserEntity userEntity = userRepository.findByUserName(userName);
            if (isCurrentPasswordMatchWithSystem(currentPassword, userEntity.getPassword())) {
                String hashPassword = CommonUtil.get_SHA_512_SecurePassword(newPassword);
                userEntity.setPassword(hashPassword);
                userRepository.save(userEntity);
            } else {
                throw new ServiceException("Current password not match with system");
            }
        } catch (Exception e) {
            logger.error("updatePassword() failed to update password userName :" + userName, e);
            throw new ServiceException(e);
        }
        logger.info("updatePassword() end userName :" + userName);
    }

    private boolean isCurrentPasswordMatchWithSystem(String currentPassword, String password) {
        String hashPassword = CommonUtil.get_SHA_512_SecurePassword(currentPassword);
        return hashPassword.equals(password) ? true : false;
    }

    @Override
    public User updateUser(User user) throws ServiceException {
        logger.info("updateUser() start input :" + user);
        try {
            UserEntity userEntity = userRepository.findByUserName(user.getUserName());
            UserEntity userEntityForUpdate = commonUtil.prepareUserEntityForUpdate(userEntity, user);
            UserEntity updatedUserEntity = userRepository.save(userEntityForUpdate);
            return commonUtil.convertToUser(updatedUserEntity, null);
        } catch (Exception e) {
            logger.error("updateUser() failed to update user :" + user);
            throw new ServiceException(e);
        }
    }

    @Override
    public List<User> getAllUser() throws ServiceException {
        logger.info("getAllUser() start ");
        List<User> userList = new ArrayList<>();
        try {
            List<UserEntity> userEntityList = (List<UserEntity>) userRepository.findAll();

            for (UserEntity userEntity : userEntityList) {
                userList.add(commonUtil.convertToUser(userEntity));
            }
        } catch (Exception e) {
            logger.error("getAllUser() failed to get data ", e);
            throw new ServiceException(e);
        }
        logger.info("getAllUser() end");
        return userList;
    }

    @Override
    public User getUser(String userName) throws ServiceException {
        logger.info("getUser() start userName: " + userName);
        try {
            UserEntity userEntity = userRepository.findByUserName(userName);
            return commonUtil.convertToUser(userEntity);
        } catch (Exception e) {
            logger.error("getUser() failed for userName: " + userName, e);
            throw new ServiceException(e);
        }
    }
}
