package com.ums.service;

import com.ums.dto.User;
import com.ums.exception.ServiceException;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public interface UserService {

    public User createUser(User user) throws ServiceException;

    public User login(String username, String password) throws ServiceException;

    public void updatePassword(String username, String currentPassword, String newPassword) throws ServiceException;

    public User updateUser(User user) throws ServiceException;

    public List<User> getAllUser() throws ServiceException;

    public User getUser(String username) throws ServiceException;

}
