package com.ums.repository;

import com.ums.domain.UserEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<UserEntity,Long> {

    public UserEntity findByUserName(String userName) ;
}
