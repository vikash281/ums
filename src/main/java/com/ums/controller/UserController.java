package com.ums.controller;

import com.ums.dto.CreateUserRequest;
import com.ums.dto.LoginRequest;
import com.ums.dto.UpdatePasswordRequest;
import com.ums.dto.User;
import com.ums.exception.ServiceException;
import com.ums.service.UserService;
import com.ums.validator.PasswordValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
public class UserController {

    private Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService userService;

    @PostMapping(value = "/create",
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<User> createUser(@Valid @RequestBody CreateUserRequest createUserRequest) throws ServiceException {
        logger.info("createUser() api start input :" + createUserRequest);
        return new ResponseEntity<User>(userService.createUser(populateUser(createUserRequest)),
                HttpStatus.OK);
    }

    @PostMapping(value = "/login",
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<User> login(@Valid @RequestBody LoginRequest loginRequest) throws ServiceException {
        logger.info("login() api start input :" + loginRequest);
        return new ResponseEntity<User>(userService.login(loginRequest.getUserName(), loginRequest.getPassword()),
                HttpStatus.OK);
    }

    @PutMapping(value = "/updatePassword",
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity updatePassword(@Valid @RequestBody UpdatePasswordRequest updatePasswordRequest) throws ServiceException {
        logger.info("updatePassword() api start input :" + updatePasswordRequest);
        if(!PasswordValidator.isValid(updatePasswordRequest.getNewPassword())){
            throw new ServiceException("password should be at least 8 chars long.\n " +
                    "Should have 1 alphabet \n" +
                    "Should have 1 numeral \n" +
                    "Should have 1 special char");
        }
        userService.updatePassword(updatePasswordRequest.getUserName(),
                updatePasswordRequest.getCurrentPassword(),
                updatePasswordRequest.getNewPassword());
        return new ResponseEntity(HttpStatus.OK);
    }

    @PutMapping(value = "/updateUser",
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<User> updateUser(@Valid @RequestBody CreateUserRequest updateUserRequest) throws ServiceException {
        logger.info("updateUser() api start input :" + updateUserRequest);
        User user = userService.updateUser(populateUser(updateUserRequest));
        logger.info("updateUser() api end output :" + user);
        return new ResponseEntity(user, HttpStatus.OK);
    }

    @GetMapping(value = "/user/{userName}",
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE}
    )
    public ResponseEntity<User> getUserByName(@PathVariable("userName") String userName) throws ServiceException {
        logger.info("getUserByName() api start input userName :" + userName);
        User user = userService.getUser(userName);
        logger.info("getUserByName() api end output user :" + user);
        return new ResponseEntity(user, HttpStatus.OK);
    }

    @GetMapping(value = "/users",
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<List<User>> getAllUser() throws ServiceException {
        logger.info("getAllUser() api start ");
        List<User> userList = userService.getAllUser();
        logger.info("getAllUser() api end ");
        return new ResponseEntity(userList, HttpStatus.OK);
    }

    private User populateUser(CreateUserRequest createUserRequest) {
        User user = new User();
        user.setUserName(createUserRequest.getUserName());
        user.setFirstName(createUserRequest.getFirstName());
        user.setLastname(createUserRequest.getLastName());
        user.setEmail(createUserRequest.getEmail());
        user.setDob(createUserRequest.getDob());
        return user;
    }

}